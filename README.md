﻿# Grunt Demo

This demo include the following branches:

- `stage1` contains the basic structure of the project and the `jsdoc` Grunt task definition.
- `stage2` contains the basic configuration for `jshint` Grunt task.
- `stage3` contains the basic configuration for `jsbeautifier` Grunt task. 
