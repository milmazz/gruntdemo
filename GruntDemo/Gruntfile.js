﻿module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jsdoc: {
            dist: {
                src: ['tag.js'],
                dest: 'doc'
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: ['tag.js']
        },
        jsbeautifier: {
            modify: {
                src: 'tag.js',
                options: {
                    config: '.jsbeautifyrc'
                }
            },
            verify: {
                src: ['tag.js'],
                options: {
                    mode: 'VERIFY_ONLY',
                    config: '.jsbeautifyrc'
                }
            }
        }
    });

    // Load the plugin that provides the 'jsdoc' task.
    grunt.loadNpmTasks('grunt-jsdoc');

    // Load the plugin that provides the 'jshint' task.
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // Load the plugin that provides the 'jsbeautifier' task.
    grunt.loadNpmTasks('grunt-jsbeautifier');
};
