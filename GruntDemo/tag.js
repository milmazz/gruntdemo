/**
 * @file Working with Tags
 * @author John Doe <john.doe@example.com>
 * @version 0.1
 */

var Tag = $(function () {
    /**
     * The Tag definition.
     *
     * @param {String} id - The ID of the Tag.
     * @param {String} description - Concise description of the tag.
     * @param {Number} min - Minimum value accepted for trends.
     * @param {Number} max - Maximum value accepted for trends.
     * @param {Object} plc - The ID of the {@link PLC} object where this tag belongs.
     */
    var Tag = function (id, description, min, max, plc) {
        id = id;
        description = description;
        trend_min = min;
        trend_max = max;
        plc = plc;
    };

    return {
        /**
         * Get the current value of the tag.
         *
         * @see [Example]{@link http://example.com}
         * @returns {Number} The current value of the tag.
         */
        getValue: function () {
            return Math.random;
        }
    };
}());
